Track Runner Linked List

By _Nicholas Ayers_

Extract the code and directory structure using:

    tar -xvf Ayers_hw2.tar

To compile this ensure the following contents of the working directory: 

- Provided src/ and inc/ directories are present

- Empty build/ directory is present

- Provided makefile is present

To compile the main interactive shell script run:
  
    make

To compile the Unit Tests run:
   
    make test

The Unit Tests just run built in tests and print the results.

    ./tester.exe

The interactive shell script will ask for user input (1-5), Name, School, bib_number, time etc.

There is a provided tester.txt file to be passed to the shell script as input for a short demonstration.

At the start of each loop it will print the current list

    ./main.exe
