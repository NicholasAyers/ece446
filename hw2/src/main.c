#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "struct_lists.h"


//takes user input and returns a node
struct list_node *make_node()
{
	unsigned int number = 0;
	unsigned int time = 0;
	char school[50];
	char name[50];
	printf("Name>");
	fgets(name, 20, stdin);
	printf("School>");
	fgets(school,50,stdin);
	printf("Bib Number>");
	scanf("%d", &number);
	getchar();
	printf("Time>");
	scanf("%d", &time);
	getchar();

	school[strcspn(school, "\n")] = 0;
	name[strcspn(name, "\n")] = 0;
	struct list_node *node = create_node(number, name, school, time);
	
	
	return node;
	
}
//gets user input and removes a node at an index
void remove_node(struct list_node **base) {
	unsigned int index = 0;
	if (!base) {
		printf("Can not remove entry from empty list");
	}
	printf("Lane#>");
	scanf("%d", &index);
	if (index && remove_entry(base, --index) == -1) {
		printf("Invalid removal operation please try again\n");
	}	
}

//Main loop
//takes an input for selection and logic to create and display list
int main(){
	int selection = 0;
	struct list_node *base_node = NULL;
	struct list_node *new_node = NULL;
	while (1) {
		selection = 0;
		new_node = NULL;
		if (base_node) {
			printf("\n");
			print_list(base_node);
			printf("----------------\n");
		}
		printf("1:Push\n2:Insert\n3:Delete\n4:Clear\n5:Quit\n>");
		scanf("%d", &selection);
		getchar();
		switch(selection){
		case 1:
			new_node = make_node();
			if (!base_node) {
				base_node = new_node;
			}else{
				push_entry(base_node, new_node);
			}
			break;
		case 2:
			if (!base_node) {
				printf("Can not insert to empty list(use push)\n");
				break;
			}
			new_node = make_node();
			int index = 0;
			printf("Lane>");
			scanf("%d", &index);
			
			if (!index || insert_entry(&base_node, --index, new_node)) {
				printf("Invalid insert please re-check arguments\n");
			}
			break;
		case 3:
			if (!base_node) {
				printf("Can not delete from empty list\n");
				break;
			}
			remove_node(&base_node);
			break;
			
		case 4:
			clear_list(base_node);
			base_node = NULL;
			break;
		case 5:
			clear_list(base_node);
			return 0;
		default:
			continue;
		}
	}
	return 0;
}
