#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "struct_lists.h"

#define NAMES ((char *[]){"Nicholas", "Shawndra", "Defy", "Nico", "Nass", "Warchief", "Okusama", "Deco", "Nify"})

#define SCHOOLS ((char *[]){ "GMU", "CNU", "VT", "ODU"})

struct list_node *create_list(unsigned int count)
{
	if (count == 0) {
		return NULL;
	}
	struct list_node *base = create_node(0, NAMES[0], SCHOOLS[0], 10);
	struct list_node *new = NULL;
	if (!base) {
		return NULL;
	}
	for (unsigned int i=1;i<count;i++) {
		new = create_node(i, NAMES[i%9],SCHOOLS[i%4],i*10);
		push_entry(base,new);		
	}
	return base;
}

/*
This function will check all the variables of a node against the expected values
*/
int node_is_valid(struct list_node *node, unsigned int number, char *name, char *school, unsigned int time)
{
	if (!node) {
		return 0;
	}
	if(node->bib_number != number || node->time_in_seconds != time) {
		return 0;
	}
	if((!name && node->name) || (name && !(node->name))) {
		return 0;
	}else{
		if(strcmp(node->name, name)) {
			return 0;
		}
	}
	if((!school && node->school) || (school && !(node->school))) {
		return 0;
	}else{
		if(strcmp(node->school, school)){
			return 0;
		}
	}
	return 1;
}
/* 
This functions tests the functionality of the create_node function.
Tt assumes delete_node has already been trested.
If not this may lead to memory leaks but tests should still be valid.
*/
int test_create_node()
{
	int total_failures = 0;
	struct list_node *node;
	int result;

	printf("create_node tests:\n");
	
	/*trivial case*/
	node = create_node(0, "", "", 0);
	result = node_is_valid(node, 0, "", "", 0);
	delete_node(node);
	node = NULL;
	total_failures += !result;
	printf("\tTest 1 (Trivial Case) \t: %s\n", result ? "PASS" : "FAIL");

	/*null pointer 1*/
	node = create_node(0, NULL, "", 0);
	result = node_is_valid(node, 0, "", "", 0);
	delete_node(node);
	node = NULL;
	total_failures += !result;
	printf("\tTest 2 (null pointer 1) : %s\n", result ? "PASS" : "FAIL");

	/*null pointer 2*/
	node = create_node(0, "", NULL, 0);
	result = node_is_valid(node, 0, "", "", 0);
	delete_node(node);
	node = NULL;
	total_failures += !result;
	printf("\tTest 3 (null pointer 2) : %s\n", result ? "PASS" : "FAIL");

	/*full test*/
	node = create_node(666, "Nicholas Ayers", "George Mason", 75);
	result = node_is_valid(node, 666, "Nicholas Ayers", "George Mason", 75);
	delete_node(node);
	total_failures += !result;
	printf("\tTest 4 (Full Test) \t: %s\n", result ? "PASS" : "FAIL");


	
	return total_failures;
	
}

int test_push()
{
	printf("push_entry tests:\n");
	int total_failures = 0;
	int result = 0;
	int r;
	struct list_node *base = NULL;
	struct list_node *node = NULL;
	base = create_node(1, "Nicholas", "GMU", 10);
	
	/*first element test*/
	node = create_node(2, "Defy", "OSU", 11);
	r = push_entry(base, node);
	if (!node || r != 0 || base->next != node || node->prev != base) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest1 (1st element) \t: %s\n", result == 0 ? "Pass" : "Fail");
	node = NULL;
      	result = 0;

	/*second element test*/
	node = create_node(3, "Okusama", "UVA", 12);
	r = push_entry(base, node);
	if (!node || r != 0 || base->next->next != node || node->prev->prev != base) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest2 (2nd element) \t: %s\n", result == 0 ? "Pass" : "Fail");
	delete_node(node);
	node = NULL;
	delete_node(base->next);
	base->next = NULL;
	result = 0;
	
	/*empty base*/
	node = create_node(4, "Shawndra", "CNU", 13);
	r = push_entry(NULL, node);

	if (!node || r != 1 || base->next != NULL || node->prev != NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest3 (empty base) \t: %s\n", result == 0 ? "Pass" : "Fail");
	delete_node(node);
	base->next = NULL;
	node = NULL;
	result = 0;
	
	/* empty node*/
	r = push_entry(base, NULL);
	if (node || r != 2 || base-> next != NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest4 (empty node) \t: %s\n", result == 0 ? "Pass" : "Fail");
	base->next = NULL;
	node = NULL;
	result = 0;
	delete_node(base);

	return total_failures;
}

int test_insert()
{
	printf("insert_entry tests:\n");

	int total_failures = 0;
	int result = 0;
	int r = 0;
	struct list_node *base = NULL;
	struct list_node *node = NULL;
	base = create_node(7, "Nicholas Ayers", "GMU", 60);
	if (!base) {
		return 1;
	}
	/*insert element at end*/
	node = create_node(13, "Defy", "OSU", 66);
	r = insert_entry(&base, 1, node);
	if (!node || r != 0 || base->next != node || node->prev != base) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest1 (insert_end) \t: %s\n", result == 0 ? "Pass" : "Fail");
	node->prev = NULL;
	base->next = NULL;
	result = 0;
	r = 0;
	
	/*insert element at start*/
	r = insert_entry(&base, 0, node);
	if (!node || r != 0 || base->prev != NULL || base->next->time_in_seconds != 60 || base != node) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest2 (insert_start) \t: %s\n", result == 0 ? "Pass" : "Fail");
	result = 0;
	r = 0;
	
	/*insert element in the middle*/
	node = create_node(26, "Shawndra", "VT", 666);
	r = insert_entry(&base, 1, node);
	if (!node || r != 0 || base->next != node || node->prev != base || node->prev == NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest3 (inset_middle) \t: %s\n", result == 0 ? "Pass" : "Fail");
	clear_list(base);
	node = NULL;
	result = 0;
	r = 0;
		
	/*insert element past valid index*/
	base = create_node(1, "Nicholas", "GMU", 420);
	node = create_node(3, "Nico", "CNU", 69);
	r = insert_entry(&base, 2, node);

	if (!node || r != -1 || node->prev != NULL || node->next != NULL || base->next != NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest4 (invalid_index) \t: %s\n", result == 0 ? "Pass" : "Fail");
	node->next = NULL;
	node->prev = NULL;
	base->next = NULL;
	base->prev = NULL;
	delete_node(base);
	delete_node(node);
	r = 0;
	result = 0;
	
	/*null base*/
	node = create_node(1, "Nico", "TEST", 23);
	r = insert_entry(NULL, 1, node);
	if (!node || r != 1 || node->prev != NULL || node->next != NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest5 (null base) \t: %s\n", result == 0 ? "Pass" : "Fail");
	clear_list(node);
	r = 0;
	result = 0;
	//clear_list(node);
	
	/*null node*/
	base = create_node(1, "Nicholas", "GMU", 10);
	r = insert_entry(&base, 1, NULL);
	if (r != 2 || base->prev != NULL || base->next != NULL) {
		result = 1;
		total_failures +=1;
	}
	printf("\tTest6 (null node) \t: %s\n", result == 0 ? "Pass" : "Fail");

	delete_node(base);
	
	return total_failures;
}

int test_remove()
{
	printf("remove_entry tests:\n");
	int total_failures = 0;
	int result = 0;
	struct list_node *base = NULL;

	/*Null Base*/
	if (remove_entry(&base, 0) != -1) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest1 (null base) \t: %s\n", result == 0 ? "Pass" : "Fail");
	result = 0;

	/*OutOfRange Index*/
	base = create_list(1);
	if (remove_entry(&base, 1) != -1) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest2 (OutOfRange) \t: %s\n", result == 0 ? "Pass" : "Fail");
	clear_list(base);
	result = 0;
	base = NULL;

	/*First Element */
	base = create_list(4);
	remove_entry(&base,0);
	if (!base || base->bib_number != 1) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest3 (1st element) \t: %s\n", result == 0 ? "Pass" : "Fail");
	result = 0;
	
	/*Middle Element*/
	remove_entry(&base, 1);
	if(!base || base->bib_number != 1 || base->next->bib_number != 3 || base->next->prev != base) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest4 (Middle Element) \t: %s\n", result == 0 ? "Pass" : "Fail");
	result = 0;
	
	/* Last Element*/
	remove_entry(&base,1);
	if(!base || base->bib_number != 1 || base->next != NULL) {
		result = 1;
		total_failures += 1;
	}
	printf("\tTest5 (Last Element) \t: %s\n", result == 0 ? "Pass" : "Fail");
	clear_list(base);
	
	return total_failures;

	
}



int main()
{
	int total = 0;
	total += test_create_node();
	total += test_push();
	total += test_insert();
	total += test_remove();
	printf("\nTotal Failures : %d\n", total);

	struct list_node *base = create_list(8);
	print_list(base);
	clear_list(base);
	
}
