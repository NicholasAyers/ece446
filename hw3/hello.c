/* 
   Made use of the scull main.c file code for registering the device 
   From O'Reily
*/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/moduleparam.h>
MODULE_LICENSE("Dual BSD/GPL");

int major_number = -1;
unsigned int minor_number = 1;
unsigned int number_of = 1;
module_param(major_number, int, S_IRUGO);
module_param(minor_number, uint, S_IRUGO);

#ifndef ALLOW_MULTIPLE
module_param(number_of, uint, S_IRUGO);
#endif

struct hello_device {
	struct cdev *cdev;
};

struct file_operations hello_ops = {
	.owner   = THIS_MODULE,
};


	

static int hello_init(void)
{
	int result = 0;
	dev_t dev = 0;
	if (major_number >= 0) {
		dev = MKDEV(major_number, minor_number);
		result = register_chrdev_region(dev, number_of, "hello");
	}else{
		result = alloc_chrdev_region(&dev, minor_number, number_of,
					     "hello");
		major_number = MAJOR(dev);
	}

	if (result < 0) {
		printk(KERN_WARNING "hello: can't get major %d\n", major_number);
		return result;
	}
		
	printk(KERN_WARNING "Hello, world [major# : %d, minor# : %d]\n",
	       major_number, minor_number);
	return 0;
	
}
static void hello_exit(void)
{
  printk(KERN_WARNING "Goodbye\n");
}




module_init(hello_init);
module_exit(hello_exit);
