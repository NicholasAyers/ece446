Nicholas Ayers
HW #3

This is a module that can be dynamically or statically assigned a major number and will say hello and goodbye

This module is designed to be used with the 5.10.63-1-MANJARO kernel.
To build make sure the /lib/module/5.10.63-1-MANJARO folder exists and run the make command in the directory with hello.c and the provided Makefile.
To run call "insmod hello.ko" or "insmod hello.ko major_number=x" where x is a number to be used as a major number
Either way it can be unloaded using "rmmod hello"

The $PATH used is /home/nicholasa/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/var/lib/snapd/snap/bin:/lib/modules/5.10.63-1-MANJARO/build/include/

The /lib/modules... directory can be added to your $PATH by running the comman ". ./path.txt"
If you are wondering why i made a bash script a txt it is because blackboard does not like the .sh extension and I know it doesnt matter to Linux.


To test the code I ran both forms of the insdmod command followed by "rmmod hello" as show above and ran journal -rkn to view the system messages

This is using the command "insmod hello":

Sep 26 23:32:51 hanajimaSaki kernel: audit: type=1106 audit(1632713571.668:189): pid=26915 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct="root" e>
Sep 26 23:32:51 hanajimaSaki kernel: Goodbye
Sep 26 23:32:51 hanajimaSaki kernel: audit: type=1105 audit(1632713571.641:188): pid=26915 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_open grantors=pam_limits,pam_unix,pam_permit acct="root" ex>
Sep 26 23:32:51 hanajimaSaki kernel: audit: type=1110 audit(1632713571.641:187): pid=26915 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred grantors=pam_faillock,pam_permit,pam_env,pam_faillock acct=>
Sep 26 23:32:51 hanajimaSaki kernel: audit: type=1101 audit(1632713571.641:186): pid=26915 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:accounting grantors=pam_unix,pam_permit,pam_time acct="nicholasa" e>
Sep 26 23:32:47 hanajimaSaki kernel: audit: type=1104 audit(1632713567.878:185): pid=26903 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred grantors=pam_faillock,pam_permit,pam_faillock acct="root" e>
Sep 26 23:32:47 hanajimaSaki kernel: audit: type=1106 audit(1632713567.878:184): pid=26903 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct="root" e>
Sep 26 23:32:47 hanajimaSaki kernel: Hello, world [major# : 511, minor# : 1]


and this is using "insmod hello major_number=420":

Sep 26 23:40:30 hanajimaSaki kernel: audit: type=1106 audit(1632714030.691:202): pid=27225 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct="root" e>
Sep 26 23:40:30 hanajimaSaki kernel: Goodbye
Sep 26 23:40:30 hanajimaSaki kernel: audit: type=1105 audit(1632714030.671:201): pid=27225 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_open grantors=pam_limits,pam_unix,pam_permit acct="root" ex>
Sep 26 23:40:30 hanajimaSaki kernel: audit: type=1110 audit(1632714030.664:200): pid=27225 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred grantors=pam_faillock,pam_permit,pam_env,pam_faillock acct=>
Sep 26 23:40:30 hanajimaSaki kernel: audit: type=1101 audit(1632714030.664:199): pid=27225 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:accounting grantors=pam_unix,pam_permit,pam_time acct="nicholasa" e>
Sep 26 23:40:26 hanajimaSaki kernel: audit: type=1104 audit(1632714026.934:198): pid=27222 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred grantors=pam_faillock,pam_permit,pam_faillock acct="root" e>
Sep 26 23:40:26 hanajimaSaki kernel: audit: type=1106 audit(1632714026.934:197): pid=27222 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct="root" e>
Sep 26 23:40:26 hanajimaSaki kernel: Hello, world [major# : 420, minor# : 1]


This shows that it takes a major_number successfully or can dynamically assign one. It also prints the majornumber, Hello and Goodby like it should.

Both the Makefile and the basis for the hello.c file was adapted from the source code provided in the Linux Device Driver 3rd edition Textbook

