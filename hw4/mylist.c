#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/moduleparam.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/rwsem.h>

MODULE_LICENSE("Dual BSD/GPL");

#define LAPNUMBERS 3

unsigned int number_of_tracks = 1;
int major_number = -1;

module_param(major_number, int, S_IRUGO);
module_param(number_of_tracks, uint, S_IRUGO);



/*
  Object that represents a single runner in a single lane to be placed in a linked list of runners
*/
struct runner {
	int lane_number;
	int current_lap;
	int total_time;
	int lap_times[LAPNUMBERS];
	struct list_head list;
};

struct mylist_dev {
	int power;
	struct rw_semaphore sem;
	struct cdev cdev;
	
};


struct mylist_dev *mylist_device;

struct runner *sample_runner;

LIST_HEAD(runner_list);
int runner_list_size = 0;

/*
  Creates a new runner object and initilizes its list_head
  @param lane_list, an array of lane_numbers, num_of_runners the number of runners MUST be equal to number of lanes in array
  @return *strct runner a contiguous array of runners equal in size to the rovided num_of_runners param
*/

struct runner *create_runners(int *lane_list, int num_of_runners, int *power)
{
	int i;
	int j;
	struct runner *runners;
	struct runner *new_runner;
	
	int needed_pages = (sizeof(struct runner) * num_of_runners) / PAGE_SIZE;
	*power = 0;

	if ((sizeof(struct runner) * num_of_runners) % PAGE_SIZE){
		needed_pages++;
	}
	i = 1;
	while (i < needed_pages){
		i <<= 1;
		(*power)++;
	}
	runners = (struct runner*)__get_free_pages(GFP_KERNEL, *power);

	if(!runners){
		return NULL;
	}
	
	for (i=0;i<num_of_runners;i++) {
		new_runner = (runners+i);
		new_runner->lane_number = *(lane_list + i);
		new_runner->current_lap = 0;
		for (j=0;j<LAPNUMBERS;j++){
			new_runner->lap_times[j] = 3*j;
		}
		new_runner->total_time = 6969;
		INIT_LIST_HEAD(&new_runner->list);
	}
	
	return runners;
}

/*
  Input: an initialized cdev pointer, the head of the full runner list, positon of runner to start reading from 
  what postion within the runner struct to begin reading at, how many bytes to read, and where to store the data
  OUTPUT: the number of bytes coppied
 */
int get_runner_data(struct mylist_dev *dev, struct list_head *list, int runner_num,
		     int struct_pos, int bytes, int *output)
{
	int i=0;
	struct list_head *pos;
	struct runner *current_runner;

	printk("mylist: reading %d runner data starting at %d getting %d bytes", runner_num, struct_pos, bytes);
	
	down_read(&dev->sem);
	printk("mylist: taking reader semaphore");

	if(!list){
		printk("mylist: error null list");
		up_read(&dev->sem);
		return -1;
	}
	
	list_for_each(pos, list){
		printk("start of for loop");
		current_runner = container_of(pos, struct runner, list);
		printk(KERN_ALERT "selected a  runner" );
		if(!current_runner || i != runner_num){
			current_runner = NULL;
			i++;
			continue;
		}else{
			printk(KERN_ALERT "FOUND MY RUNNER: %d", current_runner->lane_number);
			break;
		}
	}

	if(!current_runner){
		up_read(&dev->sem);
		return -1;
	}


	for(i=0;i<bytes;i++){
		int EOF = 0;
		//printk("mylist: start of forloop struct_pos(%d), lane_number(%d), i(%d)", struct_pos, current_runner->lane_number, i);
		switch(struct_pos){
			
		case 0:
			*(output+i) = current_runner->lane_number;
			struct_pos++;
			break;
		case LAPNUMBERS+1:
			*(output+i) = current_runner->current_lap;
			struct_pos++;
			break;
		case LAPNUMBERS+2:
			*(output+i) = current_runner->total_time;
			current_runner = container_of(current_runner->list.next, struct runner, list);
			if(!current_runner){
				EOF = 1;
			}else{
				struct_pos = 0;
			}
			break;
		default:
			if(struct_pos < 0 || struct_pos > LAPNUMBERS) {
				printk(KERN_ALERT "Invalid struct_pos in %s", __FUNCTION__);
				struct_pos = 0;
				break;
			}
			*(output+i) = current_runner->lap_times[struct_pos-1];
			struct_pos++;			
		}
		if(EOF){
			break;
		}
	}
	up_read(&dev->sem);
	printk("releasing reader semaphore");
	return i;
}

/*
  INPUT: initialized cdev pointer, full runner list, runner postion to start with, and new time
  OUTPUT: updates the runner with the new time, increments current_lap, and updates total_time
  Reutrn: -1 on error 0 on success
 */
int update_runner_data(struct mylist_dev *dev, struct list_head *list,
		       int runner_num, int time)
{
	int i=0;
	struct list_head *pos;
	struct runner *current_runner;
	printk("mylist: updating runner# %d with time %d", runner_num, time);
	
	down_write(&dev->sem);
	printk("mylist: writer semaphore taken");
	list_for_each(pos, list){
		current_runner = container_of(pos, struct runner, list);
		printk(KERN_ALERT "Runner:%d", current_runner->lane_number);
		if(!current_runner || i != runner_num){
			current_runner = NULL;
			i++;
			continue;
		}else{
			printk(KERN_ALERT "FOUND MY RUNNER: %d", current_runner->lane_number);
			break;
		}
	}

	if(!current_runner){
		up_write(&dev->sem);
		printk("mylist: error finding requested runner releasing semaphore");
		return -1;
	}

	if(current_runner->current_lap < 1 ||
	   current_runner->current_lap > LAPNUMBERS) {
		current_runner->current_lap = 1;
	}
	current_runner->lap_times[current_runner->current_lap-1] = time;
	current_runner->current_lap += 1;
	current_runner->total_time = 0;
	for(i=0;i<LAPNUMBERS;i++) {
		current_runner->total_time += current_runner->lap_times[i];
	}

	up_write(&dev->sem);
	printk("mylist: releasing writer semaphore");
	return 0;
}

/*
  INPUT: inode and file pointers
  if first open initializes the runner linked list with 8 runners
  then runs test cases on the update and read functions
 */
int mylist_open(struct inode *inode, struct file *filp)
{
	int i;
	int power = 0;
	int data[6];
	struct runner *new_runners;
	int LANES[8] = {1,2,3,4,5,6,7,8};
	struct mylist_dev *dev;
	printk("%s!\n", __FUNCTION__);

	dev = container_of(inode->i_cdev, struct mylist_dev, cdev);

	
	down_write(&dev->sem);
	printk(KERN_WARNING "mylist: Writer semaphore taken");
	if (!filp->private_data){
		printk("mylist: Creatung new private data");
		new_runners = create_runners(LANES, 8, &power);
		for(i=7;i>=0;i--){
			list_add(&(new_runners+i)->list, &runner_list); 
		}
		dev->power = power;
		filp->private_data = &runner_list;
		
	}
	up_write(&dev->sem);
	printk(KERN_WARNING "mylist: Writer semaphore released");

	get_runner_data(dev, (struct list_head*)filp->private_data, 1, 0, 6, data);

	for(i=0;i<6;i++){
		printk("Runner 1 byte %d: %d", i, data[i]);
	}
	update_runner_data(dev, (struct list_head*)filp->private_data, 1, 10);
	update_runner_data(dev, (struct list_head*)filp->private_data, 1, 20);
	update_runner_data(dev, (struct list_head*)filp->private_data, 1, 30);
	get_runner_data(dev, (struct list_head*)filp->private_data, 1, 0, 6, data);

	printk("Runner 1 byte %d: %d\n", 0, data[0]);
	printk("Runner 1 byte %d: %d\n", 1, data[1]);
	printk("Runner 1 byte %d: %d\n", 2, data[2]);
	printk("Runner 1 byte %d: %d\n", 3, data[3]);
	printk("Runner 1 byte %d: %d\n", 4, data[4]);
	printk("Runner 1 byte %d: %d\n", 5, data[5]);
	printk("mylist: exiting open()\n");
	
	return 0;          /* success */
}

/*
  INPUT: inode and file pointers
  Frees pages used by the program
 */
int mylist_release(struct inode *inode, struct file *filp)
{
	struct mylist_dev *dev = container_of(inode->i_cdev, struct mylist_dev, cdev);
	struct list_head *pos;
	printk("%s!\n", __FUNCTION__);

	__free_pages((struct page*)container_of(runner_list.next, struct runner, list), dev->power);
	
	list_for_each(pos, &runner_list){
		
	}
	
	
	return 0;
}


struct file_operations mylist_fops = {
        .owner = THIS_MODULE,
        .open = mylist_open,
};

/*
  Prepares the Module to be opened, selects major and minor number, inits semaphore and cdev
 */
static int list_init(void)
{
	int result = 0;
	dev_t dev = 0;
	if(major_number >=0) {
		dev = MKDEV(major_number, 1);
		result = register_chrdev_region(dev, 1, "mylist");
	} else {
		result = alloc_chrdev_region(&dev, 1, 1, "mylist");
		major_number = MAJOR(dev);
	}
	
	if (result < 0) {
		printk(KERN_WARNING "mylist: can't get major %d\n", major_number);
		return -1;
	}
	mylist_device = (struct mylist_dev*)kmalloc(sizeof(struct mylist_dev), GFP_KERNEL);
	printk("Preparing to init sem");
	init_rwsem(&mylist_device->sem);
	cdev_init(&mylist_device->cdev, &mylist_fops);

	cdev_add(&mylist_device->cdev, dev, 1);
	
	printk(KERN_ALERT "mylist: initilized %d:%d", MAJOR(dev), MINOR(dev));

	
	return 0;
	
	
}

static void list_exit(void)
{
	printk(KERN_WARNING "Goodbye\n");
}

module_init(list_init);
module_exit(list_exit)
