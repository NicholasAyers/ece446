PATH: /home/nicholasa/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/var/lib/snapd/snap/bin:/lib/modules/5.10.70-1-MANJARO/build/include/

This kernel module creates a linked list within the kernel to store information about track runners. It stores the times of each lap, total time and current lap number for each runner. There are 2 functions implemented and tested here.

The first function will read x# of bytes starting at a specified postion of a specified runner. The bytes from position 0 are (lane number, lap 1 time, lap 2 time, lap 3 time, current lap #, total_time)

The second funtion updates the time of a single lap of a runner. It updates the time of the current lap number, increments current lap number and recalculates total_time.

Both of these functions use reader/writer semaphores to prevent race conditions.

Test results are provided in Report.txt

To build and load this module:
-run the path script to update your path
-make
-sudo insmod mylist.ko

To test this module
-check what major number it loaded with using journalctl -rk
-run sudo mknod /dev/mylist c x 1 (where x is the major_number)
-run gcc runner_read.c
-./a.out
-check journalctl -rk for results

To unload the module:
-sudo rmmod mylist


By:
Nicholas Ayers
