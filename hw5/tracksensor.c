#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/moduleparam.h>
#include <linux/random.h>
//#include <linux/list.h>
#include <linux/slab.h>
//#include <linux/rwsem.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/wait.h>

MODULE_LICENSE("Dual BSD/GPL");

int major_number = -1;
module_param(major_number, int, S_IRUGO);

struct cdev sensor_cdev;

struct sensor_time{
	long start;
	long end;
	wait_queue_head_t *wq;
};

int announce_complete(struct sensor_time *stime)
{
	wait_queue_head_t *wq = stime->wq;
	if(!wq || !stime){
		return -1;
	}
	while(stime->start <= 0 || stime->end < stime->start ||
	      stime->end < jiffies){
		msleep(250);
	}

	wake_up_interruptible(wq);
	return 0;	
}

unsigned int sensor_poll(struct file *filp, poll_table *wait)
{
	struct sensor_time *stime = (struct sensor_time*)(filp->private_data);
	if(stime && stime->start > 0 &&  stime->end >= stime->start &&
	   stime-> end > jiffies){
		return (POLLIN | POLLRDNORM);
	}else{
		return 0;
	}
	
}
	
ssize_t sensor_read(struct file *filp, char __user *buff, size_t count, loff_t *offp)
{
	long time_in_jiffies;
	int time_in_seconds;
	
	struct sensor_time *stime = (struct sensor_time*)(filp->private_data);

	if(stime->start <= 0 || count < 4){
		return -EFAULT;
	}
	while(stime->end < stime->start || jiffies < stime->end){
		msleep(250);
	}
	time_in_jiffies = stime->end - stime->start;
	time_in_seconds = time_in_jiffies / HZ;

	if(copy_to_user(buff, &time_in_seconds, 4)){
		return -EFAULT;
	}

	return 4;
}

/*
  precondition: private data must be a vailid struct sensor_time pointer
  INPUT: __user buff, count and offp are not used
  RESULT: the sensor_time is reset and starts counting again
*/
ssize_t sensor_write(struct file *filp, const char __user *buff, size_t count, loff_t *offp)
{
	unsigned int i;
	
	struct sensor_time *stime = (struct sensor_time*)(filp->private_data);
	if(stime == NULL){
		return -EFAULT;
	}
	

	stime->start = jiffies;
	get_random_bytes(&i, sizeof(i));
	i = (i % 15) + 15;
	//i = (i % 5);
	stime->end = stime->start + i*HZ;
	printk(KERN_DEBUG "start: %ld, end:%ld, i: %d, HZ: %d\n", stime->start, stime->end, i, HZ);

	if (count == 8){
		if(copy_from_user(stime->wq, buff, 8)){
			printk(KERN_DEBUG "tracksensor: failed to copy waitqueue pointer");
			stime->wq = NULL;
		}
	}else{
		stime->wq = NULL;
	}

	return count;
}

int sensor_open(struct inode *inode, struct file *filp){
	struct sensor_time *stime = (struct sensor_time*) kmalloc(
		sizeof(struct sensor_time), GFP_KERNEL);
	stime->start = 0;
	stime->end = 0;
	filp->private_data = stime;
	return 0;
}




struct file_operations sensor_fops = {
	.owner = THIS_MODULE,
	.open = sensor_open,
	.read = sensor_read,
	.write = sensor_write,
	.poll = sensor_poll
};


static int sensor_init(void)
{
	int result = 0;
	dev_t dev = 0;
	int err;
	if(major_number >=0) {
		dev = MKDEV(major_number, 1);
		result = register_chrdev_region(dev, 3, "tracksensor");
	} else {
		result = alloc_chrdev_region(&dev, 0, 3, "tracksensor");
		major_number = MAJOR(dev);
	}

	
	if (result < 0) {
		printk(KERN_WARNING "tracksensor: can't get major %d\n", major_number);
		return -1;
	}

	cdev_init(&sensor_cdev, &sensor_fops);
	sensor_cdev.owner = THIS_MODULE;
	err = cdev_add(&sensor_cdev, dev, 3);
	printk("tracksensor adding cdeev: %d", err);
	
	printk(KERN_ALERT "tracksensor: initilized %d:%d", MAJOR(dev), MINOR(dev));

	return 0;
	
}

static void sensor_exit(void)
{
	printk(KERN_DEBUG "exiting sensor");
	printk("unregistering %d", major_number);
	cdev_del(&sensor_cdev);
	unregister_chrdev_region(MKDEV(major_number, 0), 3);
}

module_init(sensor_init);
module_exit(sensor_exit);
