#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/moduleparam.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/rwsem.h>
#include <linux/workqueue.h>
#include <linux/poll.h>
#include <linux/random.h>
#include <linux/delay.h>

MODULE_LICENSE("Dual BSD/GPL");

#define LAPNUMBERS 3
#define test 1

unsigned int number_of_tracks = 3;
int major_number = -1;

module_param(major_number, int, S_IRUGO);
module_param(number_of_tracks, uint, S_IRUGO);



/*
  Object that represents a single runner in a single lane to be placed in a linked list of runners
*/
struct runner {
	int lane_number;
	int current_lap;
	int total_time;
	struct file *sensor;
	int lap_times[LAPNUMBERS];
	struct list_head list;
};

struct mylist_dev {
	int power;
	struct list_head track_list;
	struct rw_semaphore sem;
	struct cdev cdev;
	wait_queue_head_t wq;
	int all_done;
};


struct mylist_dev *mylist_device;
struct workqueue_struct *global_queue;
struct work_struct new_work;
struct runner *sample_runner;

LIST_HEAD(runner_list);
int runner_list_size = 0;

/*
  Creates a new runner object and initilizes its list_head
  @param lane_list, an array of lane_numbers, num_of_runners the number of runners MUST be equal to number of lanes in array
  @return *strct runner a contiguous array of runners equal in size to the rovided num_of_runners param
*/

struct runner *create_runners(int num_of_runners, int *power)
{
	int i;
	int j;
	struct runner *runners;
	struct runner *new_runner;
	
	int needed_pages = (sizeof(struct runner) * num_of_runners) / PAGE_SIZE;
	*power = 0;

	if ((sizeof(struct runner) * num_of_runners) % PAGE_SIZE){
		needed_pages++;
	}
	i = 1;
	while (i < needed_pages){
		i <<= 1;
		(*power)++;
	}
	runners = (struct runner*)__get_free_pages(GFP_KERNEL, *power);

	if(!runners){
		return NULL;
	}
	
	for (i=0;i<num_of_runners;i++) {
		new_runner = (runners+i);
		new_runner->lane_number = i+1;
		new_runner->current_lap = 0;
		for (j=0;j<LAPNUMBERS;j++){
			new_runner->lap_times[j] = 0;
		}
		new_runner->total_time = 0;
		INIT_LIST_HEAD(&new_runner->list);
	}
	
	return runners;
}

/*
  INPUT: head of runner list, power of 2 pages that were allocated for the structs
  OUTPUT: frees the pages used by the structs and clears the list
*/
void clear_track_list(struct list_head *head, int power)
{
	if(head->next != head && head->next){
		__free_pages((struct page*)container_of(head->next, struct runner, list), power);
	}
	head->next = head;
	head->prev = head;

}

/*
  Input: an initialized cdev pointer, the head of the full runner list, positon of runner to start reading from 
  what postion within the runner struct to begin reading at, how many bytes to read, and where to store the data
  OUTPUT: the number of bytes coppied
 */
int get_runner_data(struct mylist_dev *dev, struct list_head *head, int runner_num,
		     int struct_pos, int bytes, int *output)
{
	int i=0;
	struct runner *current_runner;

	printk("mylist: reading %d runner data starting at %d getting %d bytes", runner_num, struct_pos, bytes);
	
	down_read(&dev->sem);
	printk("mylist: taking reader semaphore");

	if(!head){
		printk("mylist: error null list");
		up_read(&dev->sem);
		return -1;
	}
	
	list_for_each_entry(current_runner, head, list){
		printk("start of for loop");
		printk(KERN_DEBUG "selected a  runner %p", current_runner );
		if(!current_runner || i != runner_num){
			printk(KERN_DEBUG "wrong runner\n");
			printk(KERN_DEBUG "#:%d\n", current_runner->lane_number);
			printk(KERN_DEBUG "prev: %p, next: %p\n", current_runner->list.prev, current_runner->list.next);
			i++;
			continue;
		}else{
			printk(KERN_DEBUG "test");
			//printk(KERN_ALERT "FOUND MY RUNNER: %d", current_runner->lane_number);
			break;
		}
	}

	printk("got runner");

	if(!current_runner){
		printk(KERN_DEBUG "failed to find runner releasing semaphore");
		up_read(&dev->sem);
		return -1;
	}

	for(i=0;i<bytes;i++){
		int EOF = 0;
		//printk("mylist: start of forloop struct_pos(%d), lane_number(%d), i(%d)", struct_pos, current_runner->lane_number, i);
		switch(struct_pos){
			
		case 0:
			*(output+i) = current_runner->lane_number;
			struct_pos++;
			break;
		case LAPNUMBERS+1:
			*(output+i) = current_runner->current_lap;
			struct_pos++;
			break;
		case LAPNUMBERS+2:
			*(output+i) = current_runner->total_time;
			current_runner = container_of(current_runner->list.next, struct runner, list);
			if(!current_runner){
				EOF = 1;
			}else{
				struct_pos = 0;
			}
			break;
		default:
			if(struct_pos < 0 || struct_pos > LAPNUMBERS) {
				printk(KERN_DEBUG "Invalid struct_pos in %s", __FUNCTION__);
				struct_pos = 0;
				break;
			}else if(struct_pos >= current_runner->current_lap+1){
				*(output+i) = 0;
			}else{
				*(output+i) = current_runner->lap_times[struct_pos-1];
			}
			struct_pos++;			
		}
		if(EOF){
			break;
		}
	}
	up_read(&dev->sem);
	printk("releasing reader semaphore\n");
	return i;
}

/*
  INPUT: initialized cdev pointer, full runner list, runner postion to start with, and new time
  OUTPUT: updates the runner with the new time, increments current_lap, and updates total_time
  Reutrn: -1 on error 0 on success
 */
int update_runner_data(struct mylist_dev *dev, struct list_head *list,
		       int runner_num, int time)
{
	int i=0;
	struct list_head *pos;
	struct runner *current_runner;
	printk("mylist: updating runner# %d with time %d", runner_num, time);
	
	down_write(&dev->sem);
	printk("mylist: writer semaphore taken");
	list_for_each(pos, list){
		current_runner = container_of(pos, struct runner, list);
		printk(KERN_DEBUG "Runner:%d", current_runner->lane_number);
		if(!current_runner || i != runner_num){
			current_runner = NULL;
			i++;
			continue;
		}else{
			printk(KERN_DEBUG "FOUND MY RUNNER: %d", current_runner->lane_number);
			break;
		}
	}

	if(!current_runner){
		up_write(&dev->sem);
		printk("mylist: error finding requested runner releasing semaphore");
		return -1;
	}

	if(current_runner->current_lap < 1 ||
	   current_runner->current_lap > LAPNUMBERS) {
		current_runner->current_lap = 1;
	}
	current_runner->lap_times[current_runner->current_lap-1] = time;
	current_runner->current_lap += 1;
	current_runner->total_time = 0;
	for(i=0;i<LAPNUMBERS;i++) {
		current_runner->total_time += current_runner->lap_times[i];
	}

	up_write(&dev->sem);
	printk("mylist: releasing writer semaphore");
	return 0;
}

/*
  INPUT: offp represents which runner to start reading at
  OUTPUT: coppies runner data starting at offp and reading through untill count bytes have been read
*/
ssize_t track_read(struct file *filp, char __user *buff, size_t count, loff_t *offp)
{
	struct mylist_dev *dev = filp->private_data;
	int buffer[128];
	int runner_number = *offp/10;
	int lap_number = *offp % 10;
	printk("trackdriver: in read()\n");
	if (count > 128)
		return -EFAULT;
	
	get_runner_data(dev, &dev->track_list, runner_number, lap_number, count, buffer);

	return copy_to_user(buff, buffer, count);
}
/*
  Simulates the physical track sensor
  generates random data by having a 10% chance everytime it is called to "trigger" when it is past 10 seconds
  RETURN: returns a non negative vaule if there is still data to be read
*/
int check_runner_time(struct runner *runner)
{
	int i;
	unsigned int rnd;
	unsigned long time = 0;
	if (runner->current_lap < LAPNUMBERS) {
		printk(KERN_DEBUG "checking %d's 'sensor'\n", runner->lane_number);
		time = runner->lap_times[runner->current_lap];
		time = (jiffies/HZ) - time;
		if (time < 5)
			return 1;
		get_random_bytes(&rnd, sizeof(rnd));
		rnd %= 10;
		if (rnd >= 9) {
			printk(KERN_DEBUG "sensor triggered\n");
			runner->lap_times[runner->current_lap] = time;
			runner->total_time = 0;
			for (i=runner->current_lap;i>=0;i--) {
				runner->total_time += runner->lap_times[i];
			}
			
			printk(KERN_DEBUG "1:%d, 2:%d, 3:%d\n", runner->lap_times[0], runner->lap_times[1], runner->lap_times[3]);
			runner->current_lap += 1;
			if (runner->current_lap < LAPNUMBERS)
				runner->lap_times[runner->current_lap] = (jiffies/HZ);
			else
				return 0;
		}
	}else {
		return 0;
	}

	return 1;
	
}

/*
  Checks all the sesors for new data then puts itself in a wait queue to be woken up by a sensor
*/
void check_sensors(struct work_struct *ws)
{
	int i = 0;
	int reading = 1;
	struct runner *runner;
	printk(KERN_DEBUG "%s!\n", __FUNCTION__);
	if(!mylist_device)
		return;
	while (reading) {
		down_write(&mylist_device->sem);
		printk(KERN_DEBUG "mylist: writer semaphore taken\n");
		reading = 0;
		list_for_each_entry(runner, &mylist_device->track_list, list){
			reading |= check_runner_time(runner);
		}
		up_write(&mylist_device->sem);
		printk(KERN_DEBUG "mylist: releasing semaphore\n");
		i++;
		msleep(500);
		//return;
	}
	
}


int prepare_runner_list(struct mylist_dev *dev, int num_tracks)
{
	int i;
	struct runner *new_runners;
	struct runner *current_runner;
	
	printk(KERN_DEBUG "trackdriver: preparing new tracklist");
	INIT_LIST_HEAD(&dev->track_list);

	new_runners = create_runners(num_tracks, &dev->power);
	for (i=0;i<num_tracks;i++) {
		list_add(&(new_runners+i)->list, &dev->track_list);
	}

	
	list_for_each_entry(current_runner, &dev->track_list, list){
		current_runner->current_lap = 0;
		current_runner->lap_times[0] = (jiffies/HZ);
	}
	
	printk("gq:%p\n", global_queue);
	INIT_WORK(&new_work, check_sensors);
	queue_work(global_queue, &new_work);
	//printk("queued work\n");
	
	return 0;
		
}

/*
  INPUT: a buffer containing a single in that tells the driver the number of lanes (1-12) needed
  OUTPUT: sets up the sensors and starts them
 */
ssize_t track_write(struct file *fp, const char __user *buff, size_t count, loff_t *offp)
{
	int num_tracks = -1;
	struct mylist_dev *dev = NULL;

	printk("%s!\n", __FUNCTION__);

	
	if(!fp)
		return -EFAULT;
	dev = fp->private_data; 
	
	if(count != 4 || !dev)
		return -EFAULT;
	
	dev->all_done = 0;
	if(copy_from_user(&num_tracks, buff, 4) ||
	   num_tracks < 1 || num_tracks > 12)
		return -EFAULT;

	if (dev->power > 0) {
		clear_track_list(&dev->track_list, dev->power);
		dev->power = 0;
	}

	if(prepare_runner_list(dev, num_tracks))
		return 4;
	else
		return -EFAULT;
}

/*
  INPUT: inode and file pointers
  if first open initializes the runner linked list with 8 runners
  then runs test cases on the update and read functions
 */
int mylist_open(struct inode *inode, struct file *filp)
{
	int i;
	int power = 0;
	//int data[6];
	struct runner *new_runners;
	struct mylist_dev *dev;
	//struct list_head *head;
	printk("%s!\n", __FUNCTION__);
	
	global_queue = create_singlethread_workqueue("trackdriver_check");
	dev = container_of(inode->i_cdev, struct mylist_dev, cdev);
	
	
	down_write(&dev->sem);
	printk(KERN_DEBUG "trackdriver: Writer semaphore taken");
	if (!filp->private_data){
		INIT_LIST_HEAD(&dev->track_list);
		printk("mylist: Creatung new private data");
		new_runners = create_runners(number_of_tracks, &power);
		for(i=number_of_tracks;i>=0;i--){
			list_add(&((new_runners+i)->list), &dev->track_list);
		}
		dev->power = power;
		filp->private_data = dev;
		
	}
	dev->all_done = 0;
	up_write(&dev->sem);
	printk(KERN_DEBUG "trackdriver: Writer semaphore released");
	
	/*
	  head = &((struct mylist_dev*)filp->private_data)->track_list;
	get_runner_data(dev, head, 1, 0, 6, data);

	for(i=0;i<6;i++){
		printk("Runner 1 byte %d: %d", i, data[i]);
	}
	update_runner_data(dev, head, 1, 10);
	update_runner_data(dev, head, 1, 20);
	update_runner_data(dev, head, 1, 30);
	get_runner_data(dev, head, 1, 0, 6, data);

	printk("Runner 1 byte %d: %d\n", 0, data[0]);
	printk("Runner 1 byte %d: %d\n", 1, data[1]);
	printk("Runner 1 byte %d: %d\n", 2, data[2]);
	printk("Runner 1 byte %d: %d\n", 3, data[3]);
	printk("Runner 1 byte %d: %d\n", 4, data[4]);
	printk("Runner 1 byte %d: %d\n", 5, data[5]);
	printk("mylist: exiting open()\n");
	*/
	
	return 0;          /* success */
}

/*
  INPUT: inode and file pointers
  Frees pages used by the program
 */
int mylist_release(struct inode *inode, struct file *filp)
{
	struct mylist_dev *dev = container_of(inode->i_cdev, struct mylist_dev, cdev);
	printk("%s!\n", __FUNCTION__);

	
	
	
	
	__free_pages((struct page*)container_of(runner_list.next, struct runner, list), dev->power);
	
	
	
	return 0;
}


struct file_operations mylist_fops = {
        .owner = THIS_MODULE,
        .open = mylist_open,
	.read = track_read,
	.write = track_write,
};

/*
  Prepares the Module to be opened, selects major and minor number, inits semaphore and cdev
 */
static int list_init(void)
{
	int result = 0;
	dev_t dev = 0;
	if(major_number >=0) {
		dev = MKDEV(major_number, 1);
		result = register_chrdev_region(dev, 1, "trackdriver");
	} else {
		result = alloc_chrdev_region(&dev, 1, 1, "trackdriver");
		major_number = MAJOR(dev);
	}
	
	if (result < 0) {
		printk(KERN_ALERT "trackdriver: can't get major %d\n", major_number);
		return -1;
	}
	mylist_device = (struct mylist_dev*)kmalloc(sizeof(struct mylist_dev), GFP_KERNEL);
	printk("Preparing to init sem");
	init_rwsem(&mylist_device->sem);
	cdev_init(&mylist_device->cdev, &mylist_fops);

	cdev_add(&mylist_device->cdev, dev, 1);
	
	printk(KERN_DEBUG "trackdriver: initilized %d:%d\n", MAJOR(dev), MINOR(dev));

	
	return 0;
	
	
}

static void list_exit(void)
{
	printk(KERN_DEBUG "exiting trackdriver");
	cdev_del(&mylist_device->cdev);
	unregister_chrdev_region(MKDEV(major_number, 1), 1);
	printk(KERN_DEBUG "Goodbye\n");
}

module_init(list_init);
module_exit(list_exit)
