#include "stdlib.h"
#include "string.h"
#include "struct_lists.h"
#include "stdio.h"

/* creates a node from paramaters*/ /*TESTED*/
/*
  INPUT: bib_number, Runner name, Runner School and their time
  OUTPUT: A node loaded with data or NULL on invalid arguements
*/

struct list_node *create_node(unsigned int bib_number, char *name, char *school)
{
	struct list_node *node;
	int i;
	node = malloc(sizeof(struct list_node));
	
	if (!node) {
		return NULL;
	}
	for (i=0;i<3;i++) {
		node->time_in_seconds[i] = 0;
	}
	node->bib_number = bib_number;
	
	if (!name) {
		node->name = malloc(strlen("")+1);
		if (node->name) {
			strcpy(node->name, "");
		}
	}else{
		node->name = malloc(strlen(name)+1);
		if (node->name) {
			strcpy(node->name, name);
		}
	}
	if (!school) {
		node->school = malloc(sizeof(""));
		if (node->school) {
			strcpy(node->school, "");
		}
	}else{
		node->school = malloc(strlen(school)+1);
		if (node->school) {
			strcpy(node->school, school);
		}
	}
	node->next = NULL;
	node->prev = NULL;
	
	return node;
}

/* frees all elements of a node as well as the node */ /*TESTED*/
/*
  INPUT: Pointer to node to be deleted
  OUTPUT: -1 on error 0 on success
*/
int delete_node(struct list_node *node)
{
	if(!node) {
		return -1;
	}
	if (node->name) {
		free(node->name);
	}
	if (node->school) {
		free(node->school);
	}

	free(node);

	return 0;
}

/* select a node from a base node by index */ /*TESTED*/
/*
  INPUT: Starting node to look from and index from the starting node to get
  OUTPUT: Node at index i or NULL on error
*/
struct list_node *get_by_index(struct list_node *node, unsigned int i)
{
	if (!node) {
		return NULL;
	}
	for (unsigned int j=0;j<i;j++) {
		if(!node) {
			return NULL;
		}
		node = node->next;
	}

	return node;
}

/*push an entry to back of linked list */
/*
  INPUT: Starting node of LL and new node to push
  OUTPUT: 0 on success, 1 on missing base node, 2 on missing new node and -1 for other errors
*/ 
int push_entry(struct list_node *base, struct list_node *new)
{
	if (!base) {
		return 1;
	}else if (!new) {
		return 2;
	}

	while (base) {
		if(base->next == NULL) {
			base->next = new;
			new->prev = base;
			return 0;
		}
		base = base->next;		
	}

	return -1;			
}

/*
  Inserts a new node into a given index
  INPUT: Base node of LL, index to insert to, and new node to insert
  OUTPUT: 0 on success, 1 on NULL base, 2 on NULL new node, -1 on other errors
  NOTE DOES NOT MODIFY THE LOCATION OF THE PROVIDED BASE NODE
  IF INSERTED IN TO 0th INDEX CALLER MUST MOVE THEIR BASE POINTER
*/
int insert_entry(struct list_node **start_node, unsigned int index, struct list_node *new)
{
	if (!start_node) {
		return 1;
	}
	struct list_node *base = *start_node;
	unsigned int i = 0;
	if (!base) {
		return 1;
	}else if (!new) {
		return 2;
	}
	new->next = NULL;
	new->prev = NULL;

	if (index == 0) {
		new->next = base;
		new->prev = base->prev;
		base->prev = new;
		*start_node = new;
		return 0;
	}
	
	for (i = 0; i<index-1; i++){
		base = base->next;
		if(!base) {
			return -1;
		}
	}
	
	new->next = base->next;
	if (new->next) {
		new->next->prev = new;
	}
	base->next = new;
	new->prev = base;
	
	return 0;
}

/*
  Deletes all nodes in a LL
  INPUT: any node in a LL
  OUTPUT: 0 on success -1 on error
 */
int clear_list(struct list_node *base)
{
	if (!base) {
		return -1;
	}
	while (base->next) {
		base = base->next;
	}

	
	
	while (base) {
		if (base->prev){
			base = base->prev;
		}else{
			delete_node(base);
			base = NULL;
			return 0;
		}
		delete_node(base->next);
	}
	return 0;
	
}

/*
  Removes and deletes a node @ index i
  INPUT: base node to look from, and index to remove from base
  OUTPUT: 0 on success and -1 on error
*/
int remove_entry(struct list_node **base, unsigned int index) {
	struct list_node *node = *base;
	if (!base || !node) {
		return -1;
	}

	if (index == 0) {
		if (node->next) {
			node->next->prev = NULL;
		}
		*base = node->next;
		delete_node(node);
		return 0;
	}

	for (unsigned int i=0; i<index-1 ;i++) {
		if (node->next) {
			node = node->next;
		}else{
			return -1;
		}
	}
	if (!node->next) {
		return -1;
	}
	
	struct list_node *old = node->next;
	if(node->next) {
		node->next = node->next->next;
	}
	if(node->next) {
		node->next->prev = node;
	}

	if (old) {
		delete_node(old);
	}
	

	
	return 0;
	
}

/* prints all values of all nodes of a list
   INPUT: base node of a LL
   OUTPUT: prints all values returns nothing
*/

int print_list(struct list_node *base)
{
	int i = 1;
	unsigned int j;
	int complete = 1;
	printf("LANE#: NAME(BIB#) of SCHOOL\n");
	while (base) {
		printf("%d:%s(%d) of %s :\n", i, base->name, base->bib_number, base->school);
		for (j=0;j<LAPS;j++) {
			if (base->time_in_seconds[j])
				printf("%d,\t", base->time_in_seconds[j]);
			else if(base->current_lap == j)
				printf("**,\t");
			else
				printf("--,\t");
		}
		if(base->time_in_seconds[LAPS-1] != 0)
			printf("|%d|", base->total_time);
		else
			complete = 0;
		printf("\n");
		base = base->next;
		i++;
	}
	return complete;
}
