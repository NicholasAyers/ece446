#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "struct_lists.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#define MAX_NODES 12

//takes user input and returns a node
struct list_node *make_node()
{
	unsigned int number = 0;
	char school[50];
	char name[50];
	printf("Name>");
	fgets(name, 20, stdin);

	if (strlen(name) <= 1)
		return NULL;
	
	printf("School>");
	fgets(school,50,stdin);
	printf("Bib Number>");
	scanf("%d", &number);
	getchar();

	school[strcspn(school, "\n")] = 0;
	name[strcspn(name, "\n")] = 0;
	struct list_node *node = create_node(number, name, school);
	
	
	return node;
	
}

/*
  INPUT: pointer that will hold the number of runners added
  OUTPUT: Returns a populated user space linked list
 */
struct list_node *build_list(int *num_runners)
{
	struct list_node *base_node = NULL;
	struct list_node *new_node = NULL;
	int i = 0;
	*num_runners = 0;
	
	base_node = make_node();
	if (!base_node)
		return NULL;
	*num_runners += 1;
	for (i=1;i<MAX_NODES;i++) {
		new_node = make_node();
		if (!new_node)
			break;
		*num_runners += 1;
		push_entry(base_node, new_node);
	}
	return base_node;
}
/*
  INPUT: populated userspace runner list and an open driver fd
  Reads from the driver and updates the list with new data
 */
void update_list(struct list_node *head, int fd)
{
	int i=0;
	int j=0;
	int buffer[128];
	if (!head)
		return;
	read(fd, buffer, 128);
	while (head) {
		for (j=0;j<LAPS;j++)
			head->time_in_seconds[j] = buffer[i+j+1];
		head->current_lap = buffer[i+LAPS+1];
		head->total_time = buffer[i+LAPS+2];
		head = head->next;
		i += 6;
	}
} 
/*
  Main loop, opens driver, then contiously prints the list every 2 seconds
 */
int main(void)
{
	int num_runners = 0;
	int fd = open("/dev/trackdriver", O_RDWR);
	if(fd < 0)
		return printf("DRIVER MISSING ERROR: %d\n", errno);
	struct list_node *head =  build_list(&num_runners);
	write(fd, &num_runners, sizeof(num_runners));
	while (print_list(head) == 0) {
		sleep(2);
		printf("======================\n");
		update_list(head,fd);
	}
	close(fd);
	return 0;
}
