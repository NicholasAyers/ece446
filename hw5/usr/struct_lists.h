#ifndef LISTS_H
#define LISTS_H
#define LAPS 3

/*
This file defines the commonly used struct and declares functions that 
interact with the linked list to be included in other c files
*/

struct list_node {
	unsigned int bib_number;
	char *name;
	char *school;
	unsigned int current_lap;
	unsigned int time_in_seconds[LAPS];
	unsigned int total_time;
	
	struct list_node *next;
	struct list_node *prev;
};

struct list_node *create_node(unsigned int bib_number, char *name, char *school);
int delete_node(struct list_node *node);
struct list_node *get_by_index(struct list_node *node, unsigned int i);

int push_entry(struct list_node *base, struct list_node *new);
int insert_entry(struct list_node **base, unsigned int index, struct list_node *new);
int remove_entry(struct list_node **base, unsigned int index);
int clear_list(struct list_node *base);
int print_list(struct list_node *base);


#endif
