#!/bin/sh

driver="trackdriver"

module="trackdriver"
device="trackdriver"
mode="666"

num_devices=1


 # invoke insmod with all arguments we got
# and use a pathname, as newer modutils don't look in . by default
/sbin/insmod ./$module.ko $* || exit 1

# remove stale nodes
rm -f /dev/${device}

major=$(awk "\$2 == \"$driver\" {print \$1}" /proc/devices)

echo $major

mknod /dev/trackdriver c $major 1



# give appropriate group/permissions, and change the group.
# Not all distributions have staff, some have "wheel" instead.
group="staff"
grep -q '^staff:' /etc/group || group="wheel"

chgrp $group /dev/${device}
chmod $mode  /dev/${device}
