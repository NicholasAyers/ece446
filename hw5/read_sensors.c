#include "stdio.h"
#include "stdlib.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

int main(){
	int fd;
	int buff;
	char pathname[32];
	char n[3] = {'0','1','2'};

	int fd0 = open("/dev/tracksensor0", O_RDWR);
	int fd1 = open("/dev/tracksensor0", O_RDWR);
	int fd2 = open("/dev/tracksensor0", O_RDWR);

	write(fd0, &buff, sizeof(buff));
	write(fd1, &buff, sizeof(buff));
	write(fd2, &buff, sizeof(buff));
	
	int fds[3] = {fd0, fd1, fd2};
	
	while(1){
	  for(int i=0;i<3;i++){
	    read(fds[i], &buff, sizeof(int));
	    
	    printf("Sensor%d: %d\t", i, buff);
	  }
	  printf("\n");
	  sleep(1);
	}
}
