#include "stdio.h"
#include "stdlib.h"
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

int main(){
  int buf[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  int runners = 3 ;
  int counter;
  int fd = open("/dev/trackdriver", O_RDWR);

  printf("opened file with %d:\n", fd);
  printf("%d\n", errno);
  
  int result = read(fd, buf, 4*18);

  printf("%d, %d, %d, %d\n%d, %d, %d, %d\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);
  printf("%d, %d, %d, %d\n%d, %d, %d, %d\n", buf[8], buf[9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15]);
  printf("%d, %d\n", buf[16], buf[17]);

  printf("------------\n");

  result = write(fd, &runners, 4);
  counter = 0;
  while(1){
    counter++;
    result = read(fd, buf, 4*18);
  printf("%d, %d, %d, %d\n%d, %d, %d, %d\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]);
  printf("%d, %d, %d, %d\n%d, %d, %d, %d\n", buf[8], buf[9], buf[10], buf[11], buf[12], buf[13], buf[14], buf[15]);
  printf("%d, %d\n", buf[16], buf[17]);

  printf("-%d-\n", result);
  printf("Run %d\n ", counter);
  
  sleep(5);
  printf("==============\n");
  }
  close(fd);
  

  
  return 0;
}
